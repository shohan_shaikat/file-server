const path = require("path");
const fs = require("fs");

const dailyLimit = 1024 * 1024 * 1024 * 1024;

const trafficFilePath = path.join(__dirname, "traffic.json");
exports.rateLimiter = (req, res, next) => {
  const ip = req.ip;
  const today = new Date().toISOString().slice(0, 10);
  let traffic = {};

  if (fs.existsSync(trafficFilePath)) {
    traffic = JSON.parse(fs.readFileSync(trafficFilePath));
  }
  if (traffic[ip] && traffic[ip].date === today) {
    if (
      traffic[ip].upload + req.headers["content-length"] > dailyLimit ||
      traffic[ip].download + res.getHeader("content-length") > dailyLimit
    ) {
      return res
        .status(429)
        .json({ message: "Daily upload or download limit exceeded" });
    }

    traffic[ip].upload += req.headers["content-length"] || 0;
    traffic[ip].download += res.getHeader("content-length") || 0;
  } else {
    traffic[ip] = {
      date: today,
      upload: req.headers["content-length"] || 0,
      download: res.getHeader("content-length") || 0
    };
  }
  fs.writeFileSync(trafficFilePath, JSON.stringify(traffic));

  next();
};
