const multer = require("multer");
const storageType = process.env.STORAGE_TYPE || "local";
const { Storage } = require("@google-cloud/storage");

let upload;
if (storageType === "local") {
  const UPLOADS_DIR = process.env.UPLOADS_DIR || "./uploads";
  upload = multer({ dest: UPLOADS_DIR });
} else if (storageType === "cloud") {
  const storage = new Storage();
  const bucketName = process.env.BUCKET_NAME || "bucket-name";
  const bucket = storage.bucket(bucketName);
  upload = multer({
    storage: multer.memoryStorage(),
    fileFilter: (req, file, cb) => {
      cb(null, true);
    }
  }).single("file");
} else {
  throw new Error(`Unsupported storage type: ${storageType}`);
}

exports.uploadMiddleware = (req, res, next) => {
  upload.single("file")(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      return res.status(400).send("Error uploading file");
    } else if (err) {
      return res.status(500).send("Server error");
    }
    next();
  });
};
