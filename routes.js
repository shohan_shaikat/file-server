const express = require("express");
const router = express.Router();
const fileController = require("./controllers/FileController");
const { uploadMiddleware } = require("./middlewares/uploadMiddleware");
const { rateLimiter } = require("./middlewares/rateLimiter");

router
  .route("/files")
  .post(rateLimiter, uploadMiddleware, fileController.uploadFile);
router.route("/files/:publicKey").get(fileController.getFile);
router.route("/files/:privateKey").delete(fileController.deleteFile);

module.exports = router;
