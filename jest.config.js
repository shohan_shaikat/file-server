module.exports = {
  modulePathIgnorePatterns: [`${__dirname}/__tests__/mocks`],
  testEnvironment: "node",
  verbose: true
};
