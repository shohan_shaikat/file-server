const crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const UPLOADS_DIR = process.env.FOLDER;

const mimeTypes = {
  ".html": "text/html",
  ".css": "text/css",
  ".js": "text/javascript",
  ".json": "application/json",
  ".png": "image/png",
  ".jpg": "image/jpg",
  ".gif": "image/gif",
  ".wav": "audio/wav",
  ".mp4": "video/mp4",
  ".woff": "application/font-woff",
  ".ttf": "application/font-ttf",
  ".eot": "application/vnd.ms-fontobject",
  ".otf": "application/font-otf",
  ".svg": "image/svg+xml",
  ".pdf": "application/pdf"
};

const getMimeType = (filePath) => {
  const extension = path.extname(filePath).toLowerCase().substring(1);
  return mimeTypes["." + extension] || "application/octet-stream";
};

exports.uploadFile = async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).send("No file uploaded.");
    }
    const files = JSON.parse(fs.readFileSync("files.json"));
    const privateKey = crypto.randomBytes(16).toString("hex");
    const publicKey = crypto.randomBytes(16).toString("hex");
    const file = req.file;

    // Move file to public directory
    const fileExtension = file.originalname.split(".").pop();
    const fileName = `${publicKey}.${fileExtension}`;
    const newPath = `${UPLOADS_DIR}/${fileName}`;
    files.push({
      path: newPath,
      publicKey: publicKey,
      privateKey: privateKey
    });
    fs.writeFileSync("files.json", JSON.stringify(files));
    fs.rename(file.path, newPath, (err) => {
      if (err) {
        console.error(err);
        res
          .status(500)
          .json({ error: "Unable to move file to public directory" });
      } else {
        res.status(200).json({ privateKey, publicKey });
      }
    });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

exports.getFile = async (req, res) => {
  try {
    const { publicKey } = req.params;
    const files = JSON.parse(fs.readFileSync("files.json"));
    const file = files.find((obj) => obj.publicKey === publicKey);
    if (!file) {
      return res.status(404).send({ error: "File not found" });
    }
    const fileMimeType = getMimeType(file.path);
    const fileStream = fs.createReadStream(file.path);
    res.set({
      "Content-Type": fileMimeType,
      "Content-Disposition": `attachment; filename=${publicKey}`
    });
    fileStream.pipe(res);
  } catch (error) {}
};

exports.deleteFile = async (req, res) => {
  try {
    const privateKey = req.params.privateKey;
    let files = JSON.parse(fs.readFileSync("files.json"));
    const file = files.find((obj) => obj.privateKey === privateKey);
    if (!file) {
      return res.status(404).send({ error: "File not found" });
    }
    // Delete the file from the filesystem
    fs.unlinkSync(file.path);

    //Remove the file information from the files array
    files = files.filter((file) => file.privateKey !== privateKey);
    fs.writeFileSync("files.json", JSON.stringify(files));

    return res.send({ message: "File deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
