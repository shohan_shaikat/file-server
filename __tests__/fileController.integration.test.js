const request = require("supertest");
const app = require("../server");
const path = require("path");
let privateKey;
let publicKey;

describe("POST /files", () => {
  test("should upload a file successfully", async () => {
    const filePath = path.join(__dirname, "pay.png");
    const response = await request(app)
      .post("/files")
      .attach("file", filePath) // Replace with the path to the file you want to upload
      .set("Content-Type", "multipart/form-data");
    privateKey = response.body.privateKey;
    publicKey = response.body.publicKey;

    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty("privateKey");
    expect(response.body).toHaveProperty("publicKey");
  });
});

describe("GET /files/:publicKey", () => {
  test("should get file using public key", async () => {
    const response = await request(app).get(`/files/${publicKey}`);
    expect(response.statusCode).toBe(200);
    expect(response.headers["content-type"]).toContain("image");
  });
});

describe("DELETE /files/:privateKey", () => {
  it("should delete a file using private key", async () => {
    const response = await request(app)
      .delete(`/files/${privateKey}`)
      .expect(200);
    expect(response.statusCode).toBe(200);

    expect(response.body).toHaveProperty(
      "message",
      "File deleted successfully"
    );
  });
  it("should return 404 error for invalid private key", async () => {
    await request(app).delete("/files/invalid_private_key").expect(404);
  });
});
